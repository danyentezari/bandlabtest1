'use strict';

const {
  callJsonPlaceholder, 
  uploadToS3, 
  createLogGroup,
  createLogStream,
  getFromS3,
  getNextSequenceToken,
  putLogEvents
} = require('./models');

const 
  bucketName = 'inboxbandlab',
  bucketPrefix = 'comments';

module.exports.getComment = (event, context, callback) => {
    let fileName = bucketPrefix + '/' + context.awsRequestId + '.json';
    
    //REST call to JSONPlaceholder
    callJsonPlaceholder(
      event.pathParameters.id //comment id
    )
    //Upload JSON to S3 bucket
    .then(_message=>
      uploadToS3(
        bucketName,
        fileName,
        JSON.stringify(_message)
      ))
    //Respond with filename  
    .then(()=>
      callback(
        null, {statusCode: 200,body: fileName}
      ))}


module.exports.commentParser = (event, context, callback) => {
    
    let fileName = event.Records[0].s3.object.key;

    //Create CloudWatch log group
    createLogGroup(
      bucketName
    )
    //Create log stream (named after S3 file)
    .then(()=>
      createLogStream(
        bucketName,
        fileName
      ))
    //Get contents from file uploaded to S3
    .then(()=>
        getFromS3(bucketName, fileName)
      )
    //Create log event in log stream with content    
    .then(_message=>
      putLogEvents(
        bucketName,
        fileName,
        _message,
        Date.now()
      ))}