const AWS = require('aws-sdk'),
      s3 = new AWS.S3(),
      cloudwatchlogs = new AWS.CloudWatchLogs(),
      fetch = require('node-fetch'),
      jpURL = 'https://jsonplaceholder.typicode.com/posts/';


/**
 * Resolves JSON.
 * @arg id: String
 */
module.exports.callJsonPlaceholder = (id) =>
    new Promise ((resolve, reject) =>
        fetch(jpURL + id) 
        .then((response)=>response.json())
        .then((json)=>{
            resolve(json)
        })).catch((reason)=>console.log(reason))

/**
 * Resolves with @arg body:String
 * @arg bucket:String 
 * @arg key:String
 * @arg body:String
 */
module.exports.uploadToS3 = (bucket, key, body) => 
  new Promise ((resolve, reject)=>
        s3.putObject({
            Bucket: bucket,
            Key: key,
            Body: body
        }, (err, execData)=>{
            if (err) reject([err, err.stack]); // an error occurred
            else resolve(body) // successful response;
        })).catch((reason)=>console.log(reason))

/**
 * Resolves with data:String
 * @arg bucket:String 
 * @arg key:String
 */
module.exports.getFromS3 = (bucket, key) => 
  new Promise ((resolve, reject)=>
        s3.getObject({
            Bucket: bucket,
            Key: key
        }, (err, data)=>{
            if (err) reject([err, err.stack]); // an error occurred
            else resolve(data.Body.toString('utf-8')) // successful response;
        })).catch((reason)=>console.log(reason))

/**
 * Resolves with data
 * @arg logGroupName:String 
 */
module.exports.createLogGroup = (logGroupName) =>
    new Promise ((resolve, reject)=>
        cloudwatchlogs.createLogGroup({
            logGroupName: logGroupName
        }, (err, data)=> {
            if (err) reject([err, err.stack]); // an error occurred
            else resolve(data) // successful response;
        })).catch((reason)=>console.log(reason))

/**
 * Resolves with data
 * @arg logGroupName:String 
 * @arg logStreamName:String
 */
module.exports.createLogStream = (logGroupName, logStreamName) =>
    new Promise ((resolve, reject)=>
        cloudwatchlogs.createLogStream({
            logGroupName: logGroupName,
            logStreamName: logStreamName
        }, (err, data)=> {
            if (err) reject([err, err.stack]); // an error occurred
            else resolve(data) // successful response
        })).catch((reason)=>console.log(reason))

/**
 * Resolves with data
 * @arg logGroupName:String 
 * @arg logStreamName:String
 */
module.exports.getNextSequenceToken = (
    logGroupName, 
    logStreamNamePrefix ) =>
    new Promise ((resolve, reject)=>
        cloudwatchlogs.describeLogStreams({
            logGroupName: logGroupName,
            logStreamNamePrefix: logStreamNamePrefix,
            orderBy: 'LastEventTime',
            limit: 1
        }, (err, data) => {
            if (err) reject([err, err.stack]); // an error occurred
            else resolve(data) // successful response
        })).catch((reason)=>console.log(reason))

/**
 * Resolves with data
 * @arg logGroupName:String 
 * @arg logStreamName:String
 * @arg message:String 
 * @arg timestamp:Date.now()
 */
module.exports.putLogEvents = (
    logGroupName, 
    logStreamName, 
    message, 
    timestamp ) =>
    new Promise ((resolve, reject)=>
        cloudwatchlogs.putLogEvents({
            logEvents: [{
                message: message, /* required */
                timestamp: timestamp /* required */
            }],
            logGroupName: logGroupName, /* required */
            logStreamName: logStreamName, /* required */
            }, (err, data)=>{
                if (err) reject([err, err.stack]); // an error occurred
                else resolve(data) // successful response   
        })).catch((reason)=>console.log(reason))      
    
