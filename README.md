# BandLab Test

The Band Lab coding assignment.

## Install Dependencies and Deploy
```
npm install
```
```
serverless deploy
```

## Dependencies
* aws-sdk
* node-fetch

## Description

* Once deployed with Serverless, an S3 bucket named 'inboxbandlab' with prefix 'comments' will be created.
* Two lambda functions named 'getComment' and 'commentParser' will be registered.
* A log group named 'inboxbandlab' will be registered in CloudWatch.
* When the API endpoint is triggered, a .json file will be put into the aforementioned bucket.
* The contents of this file are generated from [JSONPlaceholder](https://jsonplaceholder.typicode.com/).
* Then, the contents of the file will be logged into streams corresponding to the .json file name.

## Author
* Dany Entezari - [http://www.danyentezari.com](http://www.danyentezari.com)

## Remarks
Thank you for taking the time to review my application!